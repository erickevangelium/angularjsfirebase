import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  /**
   * Metodo para listar todos los usuarios 
   */
  getUsuarios(){
    return this.firestore.collection("usuarios").snapshotChanges();
  }

  /**
   * crea un usuario en firebase
   * @param usuario usuario a crear
   */
  createUsuario(usuario:any){
    return this.firestore.collection("usuarios").add(usuario);
  }

  /**
   * actualiza un usuario existente en firebase
   * @param id id de la coleccion en firebase
   * @param usuario Usuario a actualizar
   */
  updateUsuario(id:any, usuario:any){
    return this.firestore.collection("usuarios").doc(id).update(usuario);
  }


  /**
   * borrar un estudiante existente en firebase
   * @param id id de la coleccion en firebase
   */
  deleteUsuario(id:any){
    return this.firestore.collection("usuarios").doc(id).delete();

  }
}
