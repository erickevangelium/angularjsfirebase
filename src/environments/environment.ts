// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,


  firebase: {
    apiKey: "AIzaSyCVOM6fAAgoOrLDNJJRa6Y0OasWZlQcNS0",
    authDomain: "tutorialfirebase-f3faf.firebaseapp.com",
    databaseURL: "https://tutorialfirebase-f3faf.firebaseio.com",
    projectId: "tutorialfirebase-9cc89",
    storageBucket: "tutorialfirebase-f3faf.appspot.com",
    messagingSenderId: "610608193158",
    appId: "1:514034157353:web:70165f4161e38378061938",
    measurementId: "G-TX4RG4KKGR"
  }

  


};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
